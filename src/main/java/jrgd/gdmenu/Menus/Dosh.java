package jrgd.gdmenu.Menus;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import me.simon.chestgui.API.ChestGui;
import me.simon.chestgui.API.parts.ChestGuiButton;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;

import java.util.Arrays;

import static jrgd.chestmenu.API.ItemInteractions.*;

public class Dosh {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(CommandManager.literal("dosh")
                .executes(Dosh::menu)
        );
    }

    private static int menu(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        ServerPlayerEntity player = ctx.getSource().getPlayer();
        player.openHandledScreen(doshMenu(player));
        return 1;
    }

    private static NamedScreenHandlerFactory doshMenu(ServerPlayerEntity playerEntity) {
        final String doshSkullUrl = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTM2ZTk0ZjZjMzRhMzU0NjVmY2U0YTkwZjJlMjU5NzYzODllYjk3MDlhMTIyNzM1NzRmZjcwZmQ0ZGFhNjg1MiJ9fX0=";
        final int[] doshId = new int[] {19088743, -1985229329, 19088743, -1985229329};
        String doshName = "§6Dosh";
        ItemStack doshGui = customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3], 1);
        NamedScreenHandlerFactory factory =
                new ChestGui()
                        .setName(new LiteralText("Dosh Conversion").formatted(Formatting.DARK_BLUE))
                        .setRowSize(1)
                        .setBackground(ItemStack.EMPTY)
                        .addButton(new ChestGuiButton( 3, doshGui)
                                .setName(new LiteralText("§9Diamonds > Dosh"))
                                .setLore(Arrays.asList(new LiteralText("§a[§eClick§a] §7Single trade"), new LiteralText("§a[§eShift §a+ §eClick§a] §71 stack of Dosh"), new LiteralText("§a[§eQ§a] §7Trade all diamonds")))
                                .setAction(SlotActionType.PICKUP,
                                        ()->trade(
                                                playerEntity,
                                                new ItemStack(Items.DIAMOND, 2),
                                                customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3],1).setCustomName(new LiteralText(doshName))))
                                .setAction(SlotActionType.QUICK_MOVE,
                                        ()->trade(
                                                playerEntity,
                                                new ItemStack(Items.DIAMOND, 128),
                                                customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3],64).setCustomName(new LiteralText(doshName))))
                                .setAction(SlotActionType.THROW,
                                        ()->fullTrade(
                                                playerEntity,
                                                new ItemStack(Items.DIAMOND, 2),
                                                customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3],1).setCustomName(new LiteralText(doshName))))

                        )
                        .addButton(new ChestGuiButton(4,new ItemStack(Items.BLUE_STAINED_GLASS_PANE))
                                .setName(new LiteralText("§aDosh Conversion"))
                                .setLore(Arrays.asList(new LiteralText("§eDiamonds > Dosh | §b2:1"), new LiteralText("§eDosh > Diamonds | §b5:7")))
                        )
                        .addButton(new ChestGuiButton(5, new ItemStack(Items.DIAMOND))
                                .setName(new LiteralText("§9Dosh > Diamonds"))
                                .setLore(Arrays.asList(new LiteralText("§a[§eClick§a] §7Single trade"), new LiteralText("§a[§eShift §a+ §eClick§a] §7~1 stack of diamonds (63)"), new LiteralText("§a[§eQ§a] §7Trade all Dosh")))
                                .setAction(SlotActionType.PICKUP,
                                        ()->trade(
                                                playerEntity,
                                                customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3],5).setCustomName(new LiteralText(doshName)),
                                                new ItemStack(Items.DIAMOND, 7)))
                                .setAction(SlotActionType.QUICK_MOVE,
                                        ()->trade(
                                                playerEntity,
                                                customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3],45).setCustomName(new LiteralText(doshName)),
                                                new ItemStack(Items.DIAMOND, 63)))
                                .setAction(SlotActionType.THROW,
                                        ()->fullTrade(
                                                playerEntity,
                                                customPlayerHead(doshSkullUrl, doshId[0], doshId[1], doshId[2], doshId[3],5).setCustomName(new LiteralText(doshName)),
                                                new ItemStack(Items.DIAMOND, 7)))
                        )
                        .build();
        return factory;
    }
}
